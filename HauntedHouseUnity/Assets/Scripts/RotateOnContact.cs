using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnContact : MonoBehaviour
{
    public AudioClip sound;

    [Range(0f, 1f)]
    public float volume;

    [Range(0.1f, 2.5f)]
    public float pitch;

    private AudioSource source;

    private bool activated; // bool used so that the tomb can only be rotated once
    private float angleFromBegin;
    public float rotationSpeed = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        activated = false;
        angleFromBegin = 0.0f;

        gameObject.AddComponent<AudioSource>();
        source = GetComponent<AudioSource>();

        source.clip = sound;
        source.volume = volume;
        source.pitch = pitch;
    }

    // Update is called once per frame
    void Update()
    {
        source.volume = volume;
        source.pitch = pitch;

        if (activated && angleFromBegin < 90) // the target position is at 90 degrees 
        {
            angleFromBegin += rotationSpeed;
            this.transform.RotateAround(this.transform.position, this.transform.up, rotationSpeed); // rotate the tomb around the vertical axis
        }
    }

    private bool IsHand(Collider other)
    {
        if (other.gameObject.name == "GrabVolumeBig" || other.gameObject.name == "GrabVolumeSmall") // used to detect any of the controllers
            return true;
        else
            return false;
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (IsHand(other))
        {
            if (OVRInput.Get(OVRInput.RawButton.Any)) // used to detect any input on the controllers
            {
                activated = true;
                GameObject.Find("ShedDoor").GetComponent<openWhenTriggered>().activated = true; // launch the openWhenTriggered script of the shed door : opens it
                PlaySound(); // play a grinding stone sound
            }
        }
    }

    public void PlaySound()
    {
        source.Play();
    }
}
