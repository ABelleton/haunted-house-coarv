using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class buttonScript : MonoBehaviour
{
    public Button bouton;
    public Image image;
    public GameObject associatedObject;

    // Start is called before the first frame update
    void Awake()
    {
        bouton = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Desactivate()
    {
        gameObject.SetActive(false);
    }

    public void Select()
    {
        Debug.LogWarning("test");
        bouton.Select();
    }

    public void ChangeObj(GameObject newobj)
    {
        associatedObject = newobj;
        image = transform.GetChild(0).GetComponent<Image>();
        image.sprite = Resources.Load<Sprite>(associatedObject.name);
    }
}
