# Project : Haunted House
Dans ce projet, nous avons pour volonte de creer un escape game dans une ambiance d'horreur. Vous voulez explorer une maison abandonnee (hantee ?), qui a l'air de contenir de nombreux secrets.   

# Chapitre I : Rentrer !
Pour rentrer dans cette maison, il va falloir commencer par y rentrer. Cependant, quelques enigmes sont necessaires pour obtenir l'acces a la maison.  

# Mecaniques implementees
- Teleportation (utilisation de la TP oculus)   
- Grab (sur l'objet a grab, il faut ajouter un collider (box, sphere, cylinder ou mesh collider), un rigidbody et un OVR Grabbable) -> possible de lancer l'objet   
- Inventaire (pour les objets grabs)   
- Musique d'ambiance     
  
# Bugs connus
- Solidite de la teleportation (possible de se teleporter a des endroit ou on ne devrait pas pouvoir, des fois lorsque l'on realise le geste la teleportation ne se realise pas)    
- Glitch des objets a travers le sol : lorqu'on grab/release l'objet, ils peuvent passer a travers le sol    

# Ambiance extérieure et travail
Afin d'avoir une ambiance de jeu stressante, nous avons reduit la lumière et place un brouillard. Ces 2 elements rendent le travail peu confortable, c'est pourquoi on vous invite à les desactiver pour modifier la scene. Pour cela:    
- la lumiere se trouve dans la scene, sous "world(unmovable) > MoonLight" puis mettre l'intensite a 1 dans l'inspetor.    
- le brouillard est un paramètre de rendu. Dans les onglets dans la barre supérieure de Unity, aller dans "Window", puis "Rendering", "Lightning", "Environment", puis cocher / decocher la case "fog".    

# Assets utilisés
Tous les assets suivants ont été téléchargés gratuitement sur l'AssetStore Unity    
- "AllSky Free - 10 Sky / Skybox Set": contient la skybox du ciel de nuit utilisé en extérieur.    
- "Oculus XR Plugin": plugin de base pour la VR sur les Quest 2.    
- "AshMoor - Lowpoly Environment by Unvik_3D": assets de décoration, contient les arbres, rochers et champignons à l'extérieur.    
- "Shed, Tools, Bridge and Fences": utilisé pour le cabanon à l'extérieur de la maison.    
- "Spooky Graveyard Pack": contient les tombes et autres assets pour le cimetière en extérieur.     
- "Terrain Textures Pack Free": contient les textures de sol et les billboard pour l'herbe.    

# Credits
https://freesound.org/people/Migfus20/sounds/646483/  ambiance music by Migfus20, under the license CC Atribution 4.0